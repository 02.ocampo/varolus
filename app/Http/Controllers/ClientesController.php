<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Clientes;
use Exception;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['clientes'] = Clientes::paginate(10);
        return view('clientes.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //Coge los datos del json y los almacena en la bd
    public function store(Request $request)
    {
        $campos = [
            'doc_id'=>'required|int',
            'id'=>'required|int',
            'cli_primerNombre'=>'required|String|max:255',
            'cli_primerApellido'=>'required|String|max:255',
            'cli_segundoApellido'=>'required|String|max:255',
            'ci_id'=>'required|int',
            'ba_id'=>'required|int',
            'cli_celular'=>'required|int',
            'cli_dirección'=>'required|String|max:255',
        ];
        $mensaje=[
            'required'=>'El :attribute es requerido',
            'doc_id.required'=>'El documento es requerido'
        ];
        $this->validate($request, $campos, $mensaje);

        $datosCliente = request()->except('_token');

        $datosCliente['cli_estado'] = 1;

        // return response()->json($datosCliente);

        try {

            Clientes::insert($datosCliente);
            return redirect('clientes')->with('mensaje', 'Cliente creado exitosamente');
        } catch (Exception $e) {

            return redirect('clientes')->with('error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $clientes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Clientes::findOrFail($id);

        if ($cliente == null || $cliente == '') {
            return redirect('clientes')->with('error', 'Cliente no encontrado');
        }

        return view('clientes.editar', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datosCliente = request()->except(['_token', '_method']);

        $datosCliente['cli_estado'] = isset($datosCliente['cli_estado']) ? 1 : 0;

        // return response()->json($datosCliente);

        try {

            Clientes::where('id', '=', $id)->update($datosCliente);
            return redirect('clientes')->with('mensaje', 'Cliente editado exitosamente');
            
        } catch (Exception $e) {
            return redirect('clientes')->with('error', 'Cliente no editado');
        }
    }

    // public function updateState($id, $cli_estado)
    // {
        
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clientes $clientes)
    {
        //
    }
}
