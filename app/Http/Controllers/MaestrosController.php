<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MaestrosController extends Controller
{
    public function index()
    {
        return view('maestros.index');
    }

    public function tipoDoc()
    {
        return view('maestros.tipoDoc');
    }
}