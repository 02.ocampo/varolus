<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Usuarios;
use Exception;


class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['usuarios'] = Usuarios::paginate(10);
        return view('usuarios.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos = [
            'doc_id' => 'required|int',
            'id' => 'required|int',
            'us_primerNombre' => 'required|String|max:255',
            'us_segundoNombre' => 'required|String|max:255',
            'us_primerApellido' => 'required|String|max:255',
            'us_segundoApellido' => 'required|String|max:255',
            'ci_id' => 'required|int',
            'ba_id' => 'required|int',
            'us_celular' => 'required|int',
            'us_direccion' => 'required|String|max:255',
            'us_estado' => 'required|int',
        ];
        $mensaje = [
            'required' => 'El :attribute es requerido'
        ];
        $this->validate($request, $campos, $mensaje);


        $datosUsuario = request()->except('_token');
        // return response()->json($datosUsuario);

        try {

            Usuarios::insert($datosUsuario);
            return redirect('usuarios')->with('mensaje', 'Usuario creado exitosamente');
        } catch (Exception $e) {

            return redirect('usuarios')->with('error', 'Usuario no creado');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(Usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuarios::findOrFail($id);

        if ($usuario == null || $usuario == '') {
            return redirect('usuarios')->with('error', 'Usuario no encontrado');
        }

        return view('usuarios.editar', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datosUsuario = request()->except(['_token', '_method']);

        try {

            Usuarios::where('id', '=', $id)->update($datosUsuario);
            return redirect('usuarios')->with('mensaje', 'Usuario editado exitosamente');
        } catch (Exception $e) {
            return redirect('usuarios')->with('error', 'Usuario no editado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuarios $usuarios)
    {
        //
    }
}
