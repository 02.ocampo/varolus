<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use Illuminate\Http\Request;
use Exception;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['productos'] = Productos::paginate(10);
        return view('productos.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos = [
            'pro_nombre'=>'required|String|max:255',
            'pro_descripcion'=>'required|String|max:255',
            'pro_precioCompra'=>'required|double',
            'pro_precioVenta'=>'required|double',
        ];
        $mensaje=[
            'required'=>'El :attribute es requerido'
        ];
        $this->validate($request, $campos, $mensaje);

        $datosProducto = request()->except('_token');

        try {

            Productos::insert($datosProducto);
            return redirect('productos')->with('mensaje', 'Producto creado exitosamente');
        } catch (Exception $e) {

            return redirect('productos')->with('error', 'Producto no creado');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Productos::findOrFail($id);

        if ($producto == null || $producto == '') {
            return redirect('productos')->with('error', 'Producto no encontrado');
        }

        return view('productos.editar', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datosProducto = request()->except(['_token', '_method']);

        try {

            Productos::where('id', '=', $id)->update($datosProducto);
            return redirect('productos')->with('mensaje', 'Producto editado exitosamente');
            
        } catch (Exception $e) {
            return redirect('productos')->with('error', 'Producto no editado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $productos)
    {
        //
    }
}
