<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {

            $table->integer('id');
            $table->string('cli_primerNombre');
            $table->string('cli_segundoNombre');
            $table->string('cli_primerApellido');
            $table->string('cli_segundoApellido');
            $table->integer('doc_id');
            $table->integer('ci_id');
            $table->integer('ba_id');
            $table->string('cli_dirección');
            $table->bigInteger('cli_celular');
            $table->bigInteger('cli_fijo');
            $table->boolean('cli_estado');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
