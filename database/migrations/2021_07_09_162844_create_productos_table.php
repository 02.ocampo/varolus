<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();

            $table->integer('prod_maestroid');
            $table->integer('ges_id');
            $table->string('pro_descripcion');
            $table->double('pro_precioCompra');
            $table->double('pro_precioVenta');
            $table->integer('pro_entradas');
            $table->integer('pro_salidas');
            $table->integer('pro_stock');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
