<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {

            $table->integer('id');
            $table->string('us_usuario');
            $table->string('us_primerNombre');
            $table->string('us_segundoNombre');
            $table->string('us_primerApellido');
            $table->string('us_segundoApellido');
            $table->integer('doc_id');
            $table->integer('us_rol');            
            $table->integer('ci_id');
            $table->integer('ba_id');
            $table->string('us_direccion');
            $table->bigInteger('us_celular');
            $table->bigInteger('cli_fijo');
            $table->boolean('us_estado');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
