@extends('layout.layout')
@section('contenido')

<!doctype html>
<html class="no-js " lang="en">

<body>

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Clientes</h2>
                </div>
            </div>
        </div>

        <div class="container-fluid">


            @if(Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ Session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{ Session::get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif


            <a href="{{'clientes/create'}}" class="btn btn-success">Crear</a>
            <br>
            <br>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Id</th>
                                            <th>Primer nombre</th>
                                            <th>Segundo nombre</th>
                                            <th>Primer apellido</th>
                                            <th>Segundo apellido</th>
                                            <th>Tipo documento</th>
                                            <th>Ciudad</th>
                                            <th>Barrio</th>
                                            <th>Dirección</th>
                                            <th>Celular</th>
                                            <th>Teléfono fijo</th>
                                            <th>Estado</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $clientes as $cliente )
                                        <tr>
                                            <td>{{ $cliente->id }}</td>
                                            <td>{{ $cliente->cli_primerNombre }}</td>
                                            <td>{{ $cliente->cli_segundoNombre }}</td>
                                            <td>{{ $cliente->cli_primerApellido }}</td>
                                            <td>{{ $cliente->cli_segundoApellido }}</td>
                                            <td>{{ $cliente->doc_id }}</td>
                                            <td>{{ $cliente->ci_id }}</td>
                                            <td>{{ $cliente->ba_id }}</td>
                                            <td>{{ $cliente->cli_dirección }}</td>
                                            <td>{{ $cliente->cli_celular }}</td>
                                            <td>{{ $cliente->cli_fijo }}</td>
                                            <td id="resp {{ $cliente->cli_estado }}">
                                                <br>
                                                @if($cliente->cli_estado == 1)
                                                <button type="button" class="btn btn-sm btn-success">Activo</button>
                                                @else
                                                <button type="button" class="btn btn-sm btn-danger">Inactivo</button>
                                                @endif
                                            </td>
                                            <!-- <td>{{ $cliente->cli_estado }}</td> -->
                                            <td>
                                                <a href="{{ url('clientes/'.$cliente->id.'/edit') }}" class="btn btn-warning">
                                                    Editar
                                                </a>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                                {!!$clientes->links()!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</body>

</html>
@endsection