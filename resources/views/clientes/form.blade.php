<!doctype html>

<html class="no-js " lang="en">

<body>

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>{{ $modo }} cliente</h2>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="card widget_2">
                <ul class="row clearfix list-unstyled m-b-0">
                    <li class="col-lg-12 col-md-12 col-sm-12">
                        <div class="body">

                            <!-- RECORRER CAMPOS VACÍOS PARA MOSTRAR ERROR (STORE-CONTROLLER) -->
                            @if(count($errors)>0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <!-- _____________________ -->

                            <div class="row">
                                <div class="col-6">
                                    <h6>Tipo documento</h6>
                                    <input name="doc_id" id="selTipoDocumento" value="{{ isset($cliente->doc_id)?$cliente->doc_id:old('doc_id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Documento</h6>
                                    <input name="id" id="inDocumento" value="{{ isset($cliente->id)?$cliente->id:old('id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Primer nombre</h6>
                                    <input name="cli_primerNombre" id="inprimerNombre" value="{{ isset($cliente->cli_primerNombre)?$cliente->cli_primerNombre:old('cli_primerNombre') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Segundo nombre</h6>
                                    <input name="cli_segundoNombre" id="insegundoNombre" value="{{ isset($cliente->cli_segundoNombre)?$cliente->cli_segundoNombre:old('cli_segundoNombre') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Primer apellido</h6>
                                    <input name="cli_primerApellido" id="inprimerApellido" value="{{ isset($cliente->cli_primerApellido)?$cliente->cli_primerApellido:old('cli_primerApellido') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Segundo apellido</h6>
                                    <input name="cli_segundoApellido" id="insegundoApellido" value="{{ isset($cliente->cli_segundoApellido)?$cliente->cli_segundoApellido:old('cli_segundoApellido') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Ciudad</h6>
                                    <input name="ci_id" id="selCiudad" value="{{ isset($cliente->ci_id )?$cliente->ci_id :old('ci_id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Barrio</h6>
                                    <input name="ba_id" id="selBarrio" value="{{ isset($cliente->ba_id)?$cliente->ba_id:old('ba_id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Celular</h6>
                                    <input name="cli_celular" id="inCelular" value="{{ isset($cliente->cli_celular)?$cliente->cli_celular:old('cli_celular') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Teléfono fijo</h6>
                                    <input name="cli_fijo" id="inTelFijo" value="{{ isset($cliente->cli_fijo)?$cliente->cli_fijo:old('cli_fijo') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Dirección</h6>
                                    <input name="cli_dirección" id="inDireccion" value="{{ isset($cliente->cli_dirección)?$cliente->cli_dirección:old('cli_dirección') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                @if( $modo != 'Crear' )
                                <div class="col-6">
                                    <h6>Estado</h6>
                                    <!-- <input name="cli_estado" id="Estado" value="{{ isset($cliente->cli_estado)?$cliente->cli_estado:old('cli_estado') }}" type="text" class="form-control" aria-label="Small"> -->

                                    <input name="cli_estado" id="checkEstado" class="form-check-input" type="checkbox" 
                                    value="{{ isset($cliente->cli_dirección)}}" {{ $cliente->cli_estado ? 'checked' : '' }}>
                                    <label class="form-check-label" for="checkEstado"></label>


                                </div>
                                @endif
                            </div>

                            <br>
                            <br>

                            <input type="submit" value="Guardar" class="btn btn-success">

                            <a href="{{'/clientes'}}" class="btn btn-warning">Regresar</a>

                        </div>
                    </li>

                </ul>
            </div>
        </div>


    </section>


</body>

</html>