@extends('layout.layout')
@section('contenido')


<form action="{{ url('/clientes') }}" method="post">

    @csrf
    @include('clientes.form', ['modo'=>'Crear'])

</div>

@endsection