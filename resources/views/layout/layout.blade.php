<!doctype html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>:: Nexa :: Layout</title>
    <link rel="stylesheet" href="/assets/layout/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/layout/assets/plugins/waitme/waitMe.css" />
    <link rel="stylesheet" href="/assets/layout/css/main.css">
    <link rel="stylesheet" href="/assets/layout/css/color_skins.css">
    <link rel="stylesheet" href="/fontawesome/css/all.css">
    <!-- <script src="https://kit.fontawesome.com/4430858604.js" crossorigin="anonymous"></script> -->
</head>

<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Cargando...</p>
            <!-- <div class="m-t-30"><img src="assets/layout/assets/images/logo.svg" width="48" height="48" alt="Nexa"></div> -->
        </div>
    </div>


    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="Buscar...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="col-12">

            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">VAROLUS</a>
            </div>

            <ul class="nav navbar-nav navbar-left">
                <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="fas fa-exchange-alt"></i></a></li>
                <li><a href="mail-inbox.html" class="inbox-btn hidden-sm-down" data-close="true"><i class="fas fa-lock"></i></a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="fas fa-search"></i></a></li>
                <li><a href="sign-in.html" class="mega-menu xs-hide" data-close="true"><i class="fas fa-power-off"></i></a></li>
            </ul>
        </div>
    </nav>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">

        <!-- Menu -->
        <div class="menu">
            <ul class="list">

                <!--glyphicon glyphicon-user-->
                <li><a href="usuarios"><i class="fas fa-user"></i><span>Usuarios</span> </a></li>
                <li><a href="clientes"><i class="fas fa-user-friends"></i><span>Clientes</span> </a></li>
                <li><a href="productos"><i class="fas fa-shopping-basket"></i><span>Productos</span> </a></li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="fas fa-shopping-cart"></i><span>Ventas</span> </a>
                    <ul class="ml-menu">
                        <li><a href="ing-venta">Ingresar venta</a></li>
                        <li><a href="abonos">Abonos</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="fas fa-clipboard-list"></i><span>Informes</span> </a>
                    <ul class="ml-menu">
                        <li><a href="factura">Factura</a></li>
                        <li><a href="ent-salidas">Entradas y salidas</a></li>
                        <li><a href="inf-abonos">Abonos</a></li>
                    </ul>
                </li>
                <li><a href="maestros"><i class="far fa-clipboard"></i><span>Maestros</span> </a></li>

            </ul>
        </div>
        <!-- #Menu -->
    </aside>

    <!-- Main Content -->
    @yield('contenido')

    <script src="/assets/layout/assets/bundles/libscripts.bundle.js"></script>
    <script src="/assets/layout/assets/bundles/vendorscripts.bundle.js"></script>
    <script src="/assets/layout/assets/bundles/countTo.bundle.js"></script>
    <script src="/assets/layout/assets/bundles/knob.bundle.js"></script>
    <script src="/assets/layout/assets/bundles/sparkline.bundle.js"></script>
    <script src="/assets/layout/assets/plugins/waitme/waitMe.js"></script>
    <script src="/assets/layout/js/pages/widgets/infobox/infobox-1.js"></script>
    <script src="/assets/layout/assets/bundles/mainscripts.bundle.js"></script>
    <script src="/assets/layout/js/pages/charts/jquery-knob.js"></script>
    <script src="/assets/layout/js/pages/charts/sparkline.js"></script>
    <script src="/assets/layout/js/pages/cards/basic.js"></script>
</body>

</html>