<!doctype html>

<html class="no-js " lang="en">

<body>

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>{{ $modo }} usuario</h2>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="card widget_2">
                <ul class="row clearfix list-unstyled m-b-0">
                    <li class="col-lg-12 col-md-12 col-sm-12">
                        <div class="body">

                            <!-- RECORRER CAMPOS VACÍOS PARA MOSTRAR ERROR (STORE-CONTROLLER) -->
                            @if(count($errors)>0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <!-- _____________________ -->

                            <div class="row">
                                <div class="col-6">
                                    <h6>Tipo documento</h6>
                                    <input name="doc_id" id="selTipoDocumento" value="{{ isset($usuario->doc_id)?$usuario->doc_id:old('doc_id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Documento</h6>
                                    <input name="id" id="inDocumento" value="{{ isset($usuario->id)?$usuario->id:old('id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Usuario</h6>
                                    <input name="us_usuario" id="inUsuario" value="{{ isset($usuario->us_usuario)?$usuario->us_usuario:old('us_usuario') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Rol</h6>
                                    <input name="us_rol" id="inRol" value="{{ isset($usuario->us_rol)?$usuario->us_rol:old('us_rol') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Primer nombre</h6>
                                    <input name="us_primerNombre" id="inprimerNombre" value="{{ isset($usuario->us_primerNombre)?$usuario->us_primerNombre:old('us_primerNombre') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Segundo nombre</h6>
                                    <input name="us_segundoNombre" id="insegundoNombre" value="{{ isset($usuario->us_segundoNombre)?$usuario->us_segundoNombre:old('us_segundoNombre') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Primer apellido</h6>
                                    <input name="us_primerApellido" id="inprimerApellido" value="{{ isset($usuario->us_primerApellido)?$usuario->us_primerApellido:old('us_primerApellido') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Segundo apellido</h6>
                                    <input name="us_segundoApellido" id="insegundoApellido" value="{{ isset($usuario->us_segundoApellido)?$usuario->us_segundoApellido:old('us_segundoApellido') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Ciudad</h6>
                                    <input name="ci_id" id="selCiudad" value="{{ isset($usuario->ci_id )?$usuario->ci_id :old('ci_id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Barrio</h6>
                                    <input name="ba_id" id="selBarrio" value="{{ isset($usuario->ba_id)?$usuario->ba_id:old('ba_id') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Celular</h6>
                                    <input name="us_celular" id="inCelular" value="{{ isset($usuario->us_celular)?$usuario->us_celular:old('us_celular') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Teléfono fijo</h6>
                                    <input name="cli_fijo" id="inTelFijo" value="{{ isset($usuario->cli_fijo)?$usuario->cli_fijo:old('cli_fijo') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Dirección</h6>
                                    <input name="us_direccion" id="inDireccion" value="{{ isset($usuario->us_direccion)?$usuario->us_direccion:old('us_direccion') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Estado</h6>
                                    <input name="us_estado" id="Estado" value="{{ isset($usuario->us_estado)?$usuario->us_estado:old('us_estado') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <br>
                            <br>


                            <input type="submit" value="Guardar" class="btn btn-success">

                            <a href="{{'/usuarios'}}" class="btn btn-warning">Regresar</a>



                        </div>
                    </li>

                </ul>
            </div>
        </div>

</body>

</html>