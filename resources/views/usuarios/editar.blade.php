@extends('layout.layout')
@section('contenido')


<form action="{{ url('/usuarios/'.$usuario->id ) }}" method="post">

    @csrf

    {{ method_field('PATCH') }}

    @include('usuarios.form', ['modo'=>'Editar'])

</div>

@endsection