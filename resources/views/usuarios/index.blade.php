@extends('layout.layout')
@section('contenido')

<!doctype html>
<html class="no-js " lang="en">

<body>

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Usuarios</h2>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            @if(Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ Session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{ Session::get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <a href="{{'usuarios/create'}}" class="btn btn-success">Crear</a>
            <br>
            <br>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Usuario</th>
                                            <th>Primer nombre</th>
                                            <th>Segundo nombre</th>
                                            <th>Primer apellido</th>
                                            <th>Segundo apellido</th>
                                            <th>Rol</th>
                                            <th>Tipo documento</th>
                                            <th>Ciudad</th>
                                            <th>Barrio</th>
                                            <th>Dirección</th>
                                            <th>Celular</th>
                                            <th>Teléfono fijo</th>
                                            <th>Estado</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $usuarios as $usuario )
                                        <tr>
                                            <td>{{ $usuario->id }}</td>
                                            <td>{{ $usuario->us_usuario }}</td>
                                            <td>{{ $usuario->us_primerNombre }}</td>
                                            <td>{{ $usuario->us_segundoNombre }}</td>
                                            <td>{{ $usuario->us_primerApellido }}</td>
                                            <td>{{ $usuario->us_segundoApellido }}</td>
                                            <td>{{ $usuario->us_rol }}</td>
                                            <td>{{ $usuario->doc_id }}</td>
                                            <td>{{ $usuario->ci_id }}</td>
                                            <td>{{ $usuario->ba_id }}</td>
                                            <td>{{ $usuario->us_dirección }}</td>
                                            <td>{{ $usuario->us_celular }}</td>
                                            <td>{{ $usuario->cli_fijo }}</td>
                                            <td>{{ $usuario->us_estado }}</td>
                                            <td>
                                                <a href="{{ url('/usuarios/'.$usuario->id.'/edit') }}" class="btn btn-warning">
                                                    Editar
                                                </a>

                                            </td>
                                        </tr>
                                        
                                        @endforeach
                                    </tbody>
                                </table>
                                {!!$usuarios->links()!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>

    </section>
</body>

</html>
@endsection