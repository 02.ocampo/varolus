@extends('layout.layout')
@section('contenido')


<form action="{{ url('/usuarios') }}" method="post">

    @csrf
    @include('usuarios.form', ['modo'=>'Crear'])

</div>

@endsection