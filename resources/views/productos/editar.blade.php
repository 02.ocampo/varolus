@extends('layout.layout')
@section('contenido')


<form action="{{ url('/productos/'.$producto->id ) }}" method="post">

    @csrf

    {{ method_field('PATCH') }}

    @include('productos.form', ['modo'=>'Editar'])

</div>

@endsection