@extends('layout.layout')
@section('contenido')

<!doctype html>
<html class="no-js " lang="en">

<body>

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Productos</h2>
                </div>
            </div>
        </div>

        <div class="container-fluid">

        @if(Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ Session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{ Session::get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif


            <a href="{{'productos/create'}}" class="btn btn-success">Crear</a>
            <br>
            <br>

        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Precio compra</th>
                                            <th>Precio venta</th>
                                            <th>Stock</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    @foreach( $productos as $producto )
                                        <tr>
                                            <td>{{ $producto->id }}</td>
                                            <td>{{ $producto->prod_maestroid }}</td>
                                            <td>{{ $producto->pro_descripcion }}</td>
                                            <td>{{ $producto->pro_precioCompra }}</td>
                                            <td>{{ $producto->pro_precioVenta }}</td>
                                            <td>{{ $producto->pro_stock }}</td>
                                            <td>
                                                <a href="{{ url('/productos/'.$producto->id.'/edit') }}" class="btn btn-warning">
                                                    Editar
                                                </a>

                                            </td>
                                        </tr>  
                                        @endforeach                              
                                    </tbody>
                                </table>
                                {!!$productos->links()!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

</body>

</html>
@endsection