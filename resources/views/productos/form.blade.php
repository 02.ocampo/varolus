<!doctype html>

<html class="no-js " lang="en">

<body>

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>{{ $modo }} producto</h2>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="card widget_2">
                <ul class="row clearfix list-unstyled m-b-0">
                    <li class="col-lg-12 col-md-12 col-sm-12">
                        <div class="body">

                        <!-- RECORRER CAMPOS VACÍOS PARA MOSTRAR ERROR (STORE-CONTROLLER) -->
                        @if(count($errors)>0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <!-- _____________________ -->

                            <div class="row">
                                <div class="col-6">
                                    <h6>Nombre</h6>
                                    <input name="pro_nombre" id="selTipoDocumento" value="{{ isset($cliente->pro_nombre)?$cliente->pro_nombre:old('pro_nombre') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                <h6>Descripción</h6>
                                    <input name="pro_descripcion" id="selTipoDocumento" value="{{ isset($cliente->pro_descripcion)?$cliente->pro_descripcion:old('pro_descripcion') }}" type="text" class="form-control" aria-label="Small">                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                <h6>Precio compra</h6>
                                    <input name="pro_precioCompra" id="selTipoDocumento" value="{{ isset($cliente->pro_precioCompra)?$cliente->pro_precioCompra:old('pro_precioCompra') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                    <h6>Precio venta</h6>
                                    <input name="pro_precioVenta" id="selTipoDocumento" value="{{ isset($cliente->pro_precioVenta)?$cliente->pro_precioVenta:old('pro_precioVenta') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h6>Stock</h6>
                                    <input name="pro_stock" id="selTipoDocumento" value="{{ isset($cliente->pro_stock)?$cliente->pro_stock:old('pro_stock') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                                <div class="col-6">
                                <h6>Cantidad</h6>
                                <input name="pro_estado" id="selTipoDocumento" value="{{ isset($cliente->pro_estado)?$cliente->pro_estado:old('pro_estado') }}" type="text" class="form-control" aria-label="Small">
                                </div>
                            </div>

                            <br>
                            <br>

                            <input type="submit" value="Guardar" class="btn btn-success">

                            <a href="{{'/productos'}}" class="btn btn-warning">Regresar</a>

                        </div>
                    </li>
                </ul>
            </div>
        </div>


    </section>


</body>

</html>