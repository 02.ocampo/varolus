@extends('layout.layout')
@section('contenido')


<form action="{{ url('/productos') }}" method="post">

    @csrf
    @include('productos.form', ['modo'=>'Crear'])

</div>

@endsection