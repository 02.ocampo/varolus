@extends('layout.layout')
@section('contenido')

<form action="{{ url('/maestros') }}" method="get">

    @csrf
    @include('maestros.form')

</div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">

            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>TIPO DOCUMENTO</th>
                                <th>ESTADO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>La milagrosa</td>
                                <td>*CHECK*</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection