@extends('layout.layout')
@section('contenido')

<!doctype html>
<html class="no-js " lang="en">

<body>

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Maestros</h2>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="row col-lg-4 col-md-12">
                                <div class="col-6">
                                    <h6>Barrio</h6>
                                </div>
                                <div class="col-6">
                                    <input class="form-check-input" type="checkbox" id="checkEstado">
                                    <label class="form-check-label" for="checkEstado"></label>
                                </div>
                            </div>
                            <div class="row col-lg-4 col-md-12">
                                <div class="col-6">
                                    <h6>Ciudad</h6>
                                </div>
                                <div class="col-6">
                                    <input class="form-check-input" type="checkbox" id="checkEstado">
                                    <label class="form-check-label" for="checkEstado"></label>
                                </div>
                            </div>
                            <div class="row col-lg-4 col-md-12">
                                <div class="col-6">
                                    <h6>Tipo documento</h6>
                                </div>
                                <div class="col-6">
                                    <input class="form-check-input" type="checkbox" id="checkEstado">
                                    <label class="form-check-label" for="checkEstado"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
</body>

</html>

@endsection